# itstack-demo-rpc
```
RPC是一种远程调用的通信协议，例如dubbo、thrift等，我们在互联网高并发应用开发时候都会使用到类似的服务。

本专题主要通过三个章节简单的实现rpc基础功能，来深入学习rpc是如何交互通信的； 
手写类似dubbo的rpc框架第一章《自定义配置xml》 
手写类似dubbo的rpc框架第二章《netty通信》 
手写类似dubbo的rpc框架第三章《rpc框架》

在这些章节中我们会学习到以下知识点； 
1、自定义xml的配置和解析 
2、netty 
3、JDKProxy 
4、客户端通过注册中心，发布和获取链接者 
5、服务的通过JDKProxy，进行调用方法
https://www.jianshu.com/c/24103b741b8e
```